import React, { Component } from "react";
import logo from "./Logo.png";
import { BrowserRouter as Router, Switch, Route,} from "react-router-dom";
import Home from '../pages/Home';
import Dashboards from '../pages/Dashboards';
import Contacts from '../pages/Contacts';
import Chat from '../pages/Chat';
import Tasks from '../pages/Tasks';

export default class Header extends Component {
    render() {
           return (
            <>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4 col-lg-3 navbar-container bg-light">
                        <nav class="navbar navbar-expand-md navbar-light">
                            <a class="navbar-brand"
                            href="/">
                            <img
                             src={logo}
                             height="50"
                             wigth="30"
                             className="d-inline-block align-left"
                             alt="Logo"
                             />MainPage
                            </a>
                            <button 
                            class="navbar-toggler" 
                            type="button" 
                            data-toggle="collapse" 
                            data-target="#navbar"
                            aria-controls="responsive-navbar-nav"
                            aria-expanded="false"
                            aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <a class="navbar-brand" href="#"></a>
                            <div class="collapse navbar-collapse" id="navbar">
                            
                                    <ul class="navbar-nav">
                                        <li class="nav-item">
                                        <a class="nav-link"
                                        href="/">Home</a>
                                        </li>
                                        <li class="nav-item">
                                        <a class="nav-link"
                                        href="/contacts">Contacts</a>
                                        </li>
                                        <li class="nav-item">
                                        <a class="nav-link"
                                        href="/dashboards">Dashboards</a>
                                        </li>
                                        <li class="nav-item">
                                        <a class="nav-link"
                                        href="/chat">Chat</a>
                                        </li>
                                        <li class="nav-item">
                                        <a class="nav-link"
                                        href="/tasks">Tasks</a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                    </div>
                </div>
            </div>
                <Router>
                   <Switch>
                       <Route exact path="/" component={Home} />
                       <Route exact path="/contacts" component={Contacts} />
                       <Route exact path="/dashboards" component={Dashboards} />
                       <Route exact path="/chat" component={Chat} />
                       <Route exact path="/tasks" component={Tasks} />
                   </Switch>
               </Router>
            </>    
           )
        };
}
